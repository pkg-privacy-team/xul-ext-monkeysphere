; make it easier for emacs users to keep a consistent style.
((js2-mode . ((js2-basic-offset . 2)
              (indent-tabs-mode . nil))))
